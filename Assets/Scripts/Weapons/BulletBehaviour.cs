﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    private GameObject weapon;
    private string weaponName;
    private float lifespan;
    private float damage;
    [SerializeField]
    private float bulletSpeed;

    void Start()
    {
        weapon = GameObject.FindGameObjectWithTag("Weapon");
        weaponName = weapon.name;
        switch (weaponName)
        {
            //S'HA DE MIRAR SI PUC FER GETCOMPONENT DE RANGED PER AIXI FER CHECK DE QUALSEVOL DELS DERIVATS DE ARMES SENSE TANTA MERDA
            case "Pistol":
                lifespan = weapon.GetComponent<Pistol>().bulletLifespan;
                damage = weapon.GetComponent<Pistol>().bulletStrength;
                break;

            case "Shotgun":
                lifespan = weapon.GetComponent<Shotgun>().bulletLifespan;
                damage = weapon.GetComponent<Shotgun>().bulletStrength;
                break;

            case "Rifle":
                lifespan = weapon.GetComponent<Rifle>().bulletLifespan;
                damage = weapon.GetComponent<Rifle>().bulletStrength;
                break;

            case "Revolver":
                lifespan = weapon.GetComponent<Revolver>().bulletLifespan;
                damage = weapon.GetComponent<Revolver>().bulletStrength;
                break;

            default:
                Destroy(gameObject);
                break;
        }
    }

    void Update()
    {
        lifespan -= Time.deltaTime;
        BulletLifespan();
        BulletMovement();
    }

    void BulletMovement()
    {
        transform.position += transform.right * Time.deltaTime * bulletSpeed;
    }

    //Nota: per fer el equivalent a mes distancia = menys damage de la realitat, podria fer el damage directament proporcional al lifespan, actiu a partir de cert temps (simulant que es un calcul de distancia)
    void BulletLifespan()
    {
        if (lifespan <= 0.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Trigger aca");
        Debug.Log(collision.gameObject.name);

        if (collision.gameObject.tag.Equals("Enemy"))
        {
            if (collision.gameObject.name.Equals("Stalker(Clone)"))
            {

                collision.gameObject.GetComponent<Stalker>().EnemyDamaged(damage);
            }
            else
            {
                collision.gameObject.GetComponent<Turret>().EnemyDamaged(damage);
            }
        }
        /* if (collision.gameObject.tag != "Player")
         {
             Debug.Log("entiendo, no es player");
             if (collision.gameObject.tag == "Enemy")
             {
                 Debug.Log("Trigger de enemigo manin");
                 //collision.GetComponent<Stalker>().EnemyDamaged(damage);
                 // collision.GetComponent<Turret>().EnemyDamaged(damage);
                 collision.gameObject.GetComponent<Turret>().EnemyDamaged(damage);
             }
             Destroy(gameObject);
         }*/
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
