﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ammo", menuName = "Ammo", order = 51)]
public class Ammo : ScriptableObject
{
    public string ammoType;
    public int ammoID;
    public Sprite ammoSprite;
}
