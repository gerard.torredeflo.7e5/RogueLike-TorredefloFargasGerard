﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoCrateScript : MonoBehaviour
{
    public Ammo[] ammoValues;
    public int ammoSelected;

    private void Start()
    {
        ammoSelected = Random.Range(0, 4);
        GetComponent<SpriteRenderer>().sprite = ammoValues[ammoSelected].ammoSprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponentInChildren<WeaponManagement>().EnableWeapon(ammoSelected);
            Destroy(gameObject);
        }
    }
}
