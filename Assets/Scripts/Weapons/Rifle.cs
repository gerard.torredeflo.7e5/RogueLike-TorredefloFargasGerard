﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Ranged
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
        if (Input.GetMouseButton(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Shoot();
        }
    }
}
