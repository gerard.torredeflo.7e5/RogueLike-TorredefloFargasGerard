﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bullet", menuName = "Bullet", order = 51)]
public class Bullet : ScriptableObject
{
    public string bulletType;
    public Sprite bulletSprite;
    public int damage;
    public float bulletSpeed;
}
