﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviourOLD : MonoBehaviour
{
    public Bullet[] bulletValues;
    float speed;
    Vector3 aimDirection;

    private void Start()
    {
        GetComponent<SpriteRenderer>().sprite = bulletValues[GameObject.FindGameObjectWithTag("Weapon").GetComponentInParent<WeaponManagement>().currentWeapon].bulletSprite;
        speed = bulletValues[GameObject.FindGameObjectWithTag("Weapon").GetComponentInParent<WeaponManagement>().currentWeapon].bulletSpeed;
        aimDirection = (GameObject.FindGameObjectWithTag("Weapon").GetComponent<Gun>().clickedPoint - transform.position).normalized;
    }

    private void Update()
    {
        transform.position += aimDirection * Time.deltaTime * speed;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
        }
        Destroy(gameObject);
    }
}
