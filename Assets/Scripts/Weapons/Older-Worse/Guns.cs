﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guns : Weapon
{
    [SerializeField]
    GameObject prefabBullet;
    public int ammo;
    public Vector3 clickedPoint;
    protected bool canShoot = true;

    protected virtual void Update()
    {
        if (Input.GetMouseButtonDown(0) & canShoot & (ammo > 0))
        {
            canShoot = false;
            ammo -= 1;
            clickedPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            clickedPoint.z = 0;
            Transform weaponTransform = transform.parent;
            Vector3 weaponPosition = weaponTransform.position;
            weaponPosition.x += 0.8f;
            Instantiate(prefabBullet, weaponPosition, Quaternion.identity);
            StartCoroutine(FireRate());
        }
    }

    IEnumerator FireRate()
    {
        yield return new WaitForSeconds(fireRate);
        canShoot = true;
    }
}