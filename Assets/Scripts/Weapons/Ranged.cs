﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ranged : Weapon
{
    public float bulletStrength;
    public int maxAmmo;
    public float bulletLifespan;
    public GameObject bullet;

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Shoot();
        }
    }

    protected virtual void Shoot()
    {
        //REGULAR SHOT
        Instantiate(bullet, transform.parent.position, transform.parent.rotation);
    }

    //BULLET BEHAVIOUR AMB TIMER:
    //EN COMPTES DE DISTANCIA, LES BALES ESTAN UN TEMPS DETERMINAT A ESCENA UN COP DISPARADES (I SEGONS L'ARMA CLAR)
    //https://answers.unity.com/questions/351420/simple-timer-1.html
}
