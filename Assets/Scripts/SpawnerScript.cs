﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    [SerializeField]
    private GameObject turret;
    [SerializeField]
    private GameObject stalker;
    [SerializeField]
    private GameObject cornerA;
    [SerializeField]
    private GameObject cornerB;
    private Vector3 pointA;
    private Vector3 pointB;
    private Vector3 randVect3;
    [SerializeField]
    private float timer;
    private float ogTimer;

    void Start()
    {
        pointA = cornerA.transform.position;
        pointB = cornerB.transform.position;
        ogTimer = timer;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (Random.value > 0.5)
            {
                Instantiate(turret, RandomVector3(pointA, pointB), Quaternion.identity);
            }
            else
            {
                Instantiate(stalker, RandomVector3(pointA, pointB), Quaternion.identity);
            }
            timer = ogTimer;
        }
    }

    private Vector3 RandomVector3(Vector3 pointA, Vector3 pointB)
    {
        randVect3 = new Vector3(UnityEngine.Random.Range(pointA.x, pointB.x), UnityEngine.Random.Range(pointA.y, pointB.y), UnityEngine.Random.Range(pointA.z, pointB.z));
        return randVect3;
    }

}
