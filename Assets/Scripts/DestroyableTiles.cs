﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableTiles : MonoBehaviour
{
    [SerializeField]
    private GameObject ammoCrate;
    [SerializeField]
    private GameObject lifeKit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Hay trigger de tile");
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Debug.Log("Y ademas es de Bullet");
            if (Random.value > 0.8)
            {
                Instantiate(ammoCrate);
            }
            else
            {
                Instantiate(lifeKit);
            }
            Destroy(gameObject);
        }
    }
}
