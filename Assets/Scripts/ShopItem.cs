﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItem : MonoBehaviour
{
    private GameObject go;
    private BoxCollider2D collider;

    private void Start()
    {
        go = GameObject.FindGameObjectWithTag("Score");
        collider = GetComponentInChildren<BoxCollider2D>();
        collider.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.name == ("LifeKitShop"))
        {
            Buy(50);
        }
        if (gameObject.name == ("AmmoShop"))
        {
            Buy(30);
        }
        if (gameObject.name == ("AmmoShop1"))
        {
            Buy(30);
        }
    }

    private void Buy(int cost)
    {
        if (go.GetComponent<ScoreScript>().SpendMoney(cost))
        {
            collider.enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
