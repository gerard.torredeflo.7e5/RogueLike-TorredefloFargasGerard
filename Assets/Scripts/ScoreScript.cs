﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreScript : MonoBehaviour
{
    private static ScoreScript _instance;
    public static ScoreScript Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("NullScoreScript");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    private int enemiesKilled;
    private int totalEnemiesKilled;
    [SerializeField]
    private int money = 0;
    private bool spended;

    void Update()
    {
        if (enemiesKilled >= 12)
        {
            LoadShop();
            totalEnemiesKilled = enemiesKilled;
            enemiesKilled = 0;
        }
    }

    public void LoadShop()
    {
        SceneManager.LoadScene("Shop");
    }

    public void KilledEnemy()
    {
        enemiesKilled += 1;
        money += 10;
    }

    public int CurrentMoney()
    {
        return money;
    }

    public void RepeatLevel()
    {
        SceneManager.LoadScene("Game");
    }

    public bool SpendMoney(int spent)
    {
        spended = false;
        if (money >= spent)
        {
            money -= spent;
            spended = true;
        }
        return spended;
    }
}
