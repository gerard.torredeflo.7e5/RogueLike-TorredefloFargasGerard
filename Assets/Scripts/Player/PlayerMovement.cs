﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    float speed;
    [SerializeField]
    float timer;
    float savedTimer;
    [SerializeField]
    float dashPower;
    [SerializeField]
    float dashDuration;
    bool cooldownState;
    Rigidbody2D rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        savedTimer = timer;
    }

    void Update()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(speed * inputX, speed * inputY);
        movement *= Time.deltaTime;
        transform.Translate(movement);
        if (Input.GetKeyDown("space") & !cooldownState)
        {
            //invulnerabilitat aqui
            StartCoroutine(Dash());
        }
    }

    IEnumerator Dash()
    {
        Debug.Log("dash");
        Vector2 direction = GetComponentInChildren<PlayerFacingMouse>().transform.right;
        rigidbody.velocity = direction * dashPower * Time.deltaTime;
        cooldownState = true;

        yield return new WaitForSeconds(dashDuration);
        rigidbody.velocity = direction * Time.deltaTime;

        StartCoroutine(Cooldown());
    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(timer);
        cooldownState = false;
    }

}
