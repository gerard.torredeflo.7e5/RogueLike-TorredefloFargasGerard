﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManagement : MonoBehaviour
{
    public int currentWeapon;
    int previousWeapon;
    public bool[] weaponArray;

    void Start()
    {
        previousWeapon = 0;
        currentWeapon = 0;
    }
    void Update()
    {
        SwitchWeapon();
    }

    void SwitchWeapon()
    {
        if (Input.GetKeyDown(KeyCode.C) | (Input.GetAxis("Mouse ScrollWheel") > 0f))
        {
            ScrollUp();
            SelectWeapon();
        }
        else if (Input.GetKeyDown(KeyCode.X) | (Input.GetAxis("Mouse ScrollWheel") < 0f))
        {
            ScrollDown();
            SelectWeapon();
        }
    }

    void ScrollUp()
    {
        if (currentWeapon < 3)
        {
            currentWeapon += 1;
        }
        else if (currentWeapon == 3)
        {
            currentWeapon = 0;
        }
        bool sum = true;
        AvaliableWeaponCheck(sum);
    }

    void ScrollDown()
    {
        if (currentWeapon > 0)
        {
            currentWeapon -= 1;
        }
        else if (currentWeapon == 0)
        {
            currentWeapon = 3;
        }
        bool sum = false;
        AvaliableWeaponCheck(sum);
    }

    void AvaliableWeaponCheck(bool sum)
    {
        if (!weaponArray[currentWeapon])
        {
            if (sum)
            {
                ScrollUp();
            }
            else if (!sum)
            {
                ScrollDown();
            }
        }
    }

    void SelectWeapon()
    {
        transform.GetChild(currentWeapon).gameObject.SetActive(true);
        transform.GetChild(previousWeapon).gameObject.SetActive(false);
        previousWeapon = currentWeapon;
    }

    void ActiveWeapon()
    {
        transform.GetChild(currentWeapon).gameObject.SetActive(true);
        transform.GetChild(previousWeapon).gameObject.SetActive(false);
        previousWeapon = currentWeapon;
    }

    public void EnableWeapon(int id)
    {
        if (!weaponArray[id])
        {
            weaponArray[id] = true;
            currentWeapon = id;
            ActiveWeapon();
        }
        else if (weaponArray[id])
        {
            int tempSaver = currentWeapon;
            currentWeapon = id;
            ActiveWeapon();
            switch (id)
            {
                case 0:
                    GameObject.FindGameObjectWithTag("Weapon").GetComponent<Pistol>().maxAmmo += 10;
                    break;
                case 1:
                    GameObject.FindGameObjectWithTag("Weapon").GetComponent<Shotgun>().maxAmmo += 4;
                    break;
                case 2:
                    GameObject.FindGameObjectWithTag("Weapon").GetComponent<Rifle>().maxAmmo += 15;
                    break;
                case 3:
                    GameObject.FindGameObjectWithTag("Weapon").GetComponent<Revolver>().maxAmmo += 3;
                    break;
            }
            previousWeapon = currentWeapon;
            currentWeapon = tempSaver;
            ActiveWeapon();
        }

    }
}
