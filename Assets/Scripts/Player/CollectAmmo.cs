﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectAmmo : MonoBehaviour
{
    [SerializeField]
    private GameObject weaponHolder;
    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int weaponID;
        if (collision.gameObject.CompareTag("Ammo"))
        {
            weaponID = collision.gameObject.GetComponent<AmmoCrateScript>().ammoSelected;
            if (!weaponHolder.GetComponent<WeaponManagement>().weaponArray[weaponID])
            {
                weaponHolder.GetComponent<WeaponManagement>().weaponArray[weaponID] = true;
            }

        }
    }
}
