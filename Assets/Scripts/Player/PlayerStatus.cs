﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
    [SerializeField]
    private float lifes;
    private float maxLifes;

    void Start()
    {
        lifes = HealthScript.health;
        maxLifes = lifes;
    }

    void Update()
    {
        HealthScript.health = lifes;
        if (lifes > maxLifes) lifes = maxLifes;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "LifeKit")
        {
            Destroy(collision.gameObject);
            lifes += maxLifes / 2;
        }
    }

    public void PlayerGotDamage(float damage)
    {
        lifes -= damage;
    }
}
