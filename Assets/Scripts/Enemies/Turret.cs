﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Enemies
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
        if (timeStamp <= Time.time)
        {
            Shoot();
        }
    }
    /*
    public override void EnemyDamaged(float damage)
    {
        base.EnemyDamaged(damage);
    }
    */
}
