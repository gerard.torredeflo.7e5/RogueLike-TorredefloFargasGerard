﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    float health = 10;
    [SerializeField]
    float turretShotTime;
    [SerializeField]
    GameObject prefabBullet;
    public Enemy[] enemies;
    int selectedEnemy;
    Transform playerTransform;
    int moveSpeed = 2;
    bool nowShoot;

    void Start()
    {
        selectedEnemy = Random.Range(0, 2);
        GetComponent<SpriteRenderer>().sprite = enemies[selectedEnemy].enemySprite;
        nowShoot = true;
    }

    void Update()
    {
        GetPlayerPosition();
        //transform.LookAt(playerTransform);
        transform.right = playerTransform.position - transform.position;
        if (selectedEnemy == 0)
        {
            KamikazeBehaviour();
            FixTransformEnemy();
        }
        else if (selectedEnemy == 1)
        {
            FixTransformTurret();
            TurretBehaviour();
        }
    }

    void KamikazeBehaviour()
    {
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
    }

    void TurretBehaviour()
    {
        if (nowShoot) Shoot();
    }

    void Shoot()
    {
        Debug.Log("ha disparao");
        Instantiate(prefabBullet, transform.position, Quaternion.identity);
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        nowShoot = false;
        yield return new WaitForSeconds(turretShotTime);
        nowShoot = true;
    }

    void GetPlayerPosition()
    {
        GameObject go = GameObject.Find("Player");
        playerTransform = go.transform;
    }

    void FixTransformEnemy()
    {
        var turretRotation = transform.rotation;
        turretRotation.y = 0;
        transform.rotation = turretRotation;
    }

    void FixTransformTurret()
    {
        var turretRotation = transform.rotation;
        turretRotation.y = 0;
        transform.rotation = turretRotation;
    }

    void EnemyDeath()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void EnemyDamaged(float damage)
    {
        Debug.Log("This shit got called");
        health -= damage;
    }
}
