﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBulletBehaviour : MonoBehaviour
{
    [SerializeField]
    private float lifespan;
    [SerializeField]
    private float damage;
    [SerializeField]
    private float speed;

    void Update()
    {
        lifespan -= Time.deltaTime;
        BulletLifespan();
        BulletMovement();
    }

    void BulletMovement()
    {
        transform.position += transform.right * Time.deltaTime * speed;
    }

    void BulletLifespan()
    {
        if (lifespan <= 0.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.tag == "Player")
        {
            collision.GetComponent<PlayerStatus>().PlayerGotDamage(damage);
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
