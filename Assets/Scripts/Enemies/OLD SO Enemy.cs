﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy", order = 51)]
public class Enemy : ScriptableObject
{
    public int enemyType;
    public Sprite enemySprite;
}
