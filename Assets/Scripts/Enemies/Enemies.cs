﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    public string enemyName;
    public float health;
    public float speed;
    public Transform playerTransform;
    [SerializeField]
    protected GameObject turretBullet;
    [SerializeField]
    protected float cooldown;
    protected float timeStamp;
    public GameObject ammoCrate;
    public GameObject lifeKit;


    protected virtual void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    protected virtual void Update()
    {
        Death();
        LookAtPlayer();

        //Segons l'enemic al override
        //Movement();
        //Shoot();
    }

    protected virtual void LookAtPlayer()
    {
        //PENDENT DE TESTEO
        var offset = 0f;
        Vector2 direction = playerTransform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(Vector3.forward * (angle + offset));
    }

    protected virtual void Movement()
    {
        transform.position += transform.right * Time.deltaTime * speed;
    }

    protected virtual void Shoot()
    {
        Instantiate(turretBullet, transform.position, transform.rotation);
        timeStamp = Time.time + cooldown;
    }

    protected virtual void Death()
    {
        if (health <= 0)
        {
            if (Random.value > 0.2)
            {
                Instantiate(ammoCrate, transform.position, Quaternion.identity);
            }
            else
            {
                Instantiate(lifeKit, transform.position, Quaternion.identity);
            }
            GameObject.FindGameObjectWithTag("Score").GetComponent<ScoreScript>().KilledEnemy();
            Destroy(gameObject);
        }
    }

    public virtual void EnemyDamaged(float damage)
    {
        health -= damage;
    }
}
