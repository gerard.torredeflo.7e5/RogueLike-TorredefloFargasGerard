﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBullet : MonoBehaviour
{
    [SerializeField]
    float speed;
    Transform playerTransform;

    void Start()
    {
        GetPlayerPosition();
        transform.LookAt(playerTransform);
    }

    void Update()
    {
        transform.position += transform.forward * (speed/5) * Time.deltaTime;
        FixTransform();
    }

    void GetPlayerPosition()
    {
        GameObject go = GameObject.Find("Player");
        playerTransform = go.transform;
    }

    void FixTransform()
    {
        var playerRotation = transform.rotation;
        playerRotation.x = 0;
        playerRotation.y = 0;
        playerRotation.z = 0;
        transform.rotation = playerRotation;

        var playerPosition = transform.position;
        playerPosition.z = 0;
        transform.position = playerPosition;
    }

    private void OnBecameInvisible()
    {
       // Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(collision.gameObject);
        }
        //Destroy(gameObject);
    }
}
