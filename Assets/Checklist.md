﻿## Rogue like 1

**Personatge Jugador:**  

 - [x] Movimient del jugador: moviment lliure en el pla. 
 - [ ] Animacions
 - [ ] Orientació del personatge segons posició del mouse (independent
       al moviment)
- [ ]  Verbs:  
	- [ ] Canvi d'arma:  
		- [ ]  col·lecció de fins a 5 armes.  
		- [ ]  Canvi d'arma: rodeta del mouse o X o C  
	- [ ]  Atac: segon l'arma a la que s'està  
		- [ ]  Distància:  
			- [ ]  Direcció del tret: segons posició del punter.  
			- [ ]  tipus de projectil i força segons l'arma  
			- [ ]  Nùmero de projectils  
			- [ ]  Cadencia de foc  
		- [ ] (Bonus)Melee:  
			- [ ]  Area d'impacte a 4 cuadrants: nord,surd,est,oest.  
			- [ ]  al impactar amb un enemic l'impulsa enrere. Força segons l'arma  
			- [ ]  Segons l'arma te una cadencia d'atac  
	- [ ]  Dash:  
		- [ ]  variable d'unitats d'avançament ràpid. Els enemics no fan mal durant el moviment de dash.  
		- [ ]  Ha de tenir un temps de Coldown  
		- [ ]  Impuls en linia recta segons orientació personatge  
  
	- [ ]  (Bonus) Dash + Atac: atac amb dany extra en tot enemic tocat.  
- [ ]  Barra de vida:  
	- [ ] Tamany segons la vida màxima que té el jugador  
	- [ ]  Mostra la vida actual  
- [ ]  Punts: per impacte a un enemic, mort de l'enemic, per element agafat i pantalla realitzada  
- [ ]  Escenari:  
	- [ ]  Escenari en forma d'habitació o sala gran amb mínim dos portes. Ara tancades.  
	- [ ]  crear un escenari (prefab) de proves top down amb l'eina tilemap  
	- [ ]  Elements no desrtuibles: murs, columnes. No deixen pasar, no es poden destruir.  
	- [ ]  Elements destruibles:  
		- [ ]  No diexen pasar fins que són destruits  
		- [ ] Tenen un % de probabilitats que al destruir-se deixen caura un item.  
- [ ]  Items:  
	- [ ]  Arma:  
		- [ ]  Crear Armes utilitzant herències.  
		- [ ]  Les armes item tenen un valor de bales.  
  
		- [ ]  Si s'agafa una arma que ja es té augmenta el numero de bales.  
  
	- [ ] Vida: kit de que suma vida. No suma vida si ja es té el màxim.  
- [ ]  Spawner: objecte que crea un número determinat d'enemics en un temps determinat  
	- [ ]  Enemics:  
		- [ ]  Enemic 1: persegueix al personatge i explota al estar a prop  
		- [ ]  Enemic 2: torreta que dispara al enemic.  
- [ ]  Gameplay:  
	- [ ]  Personatge comença amb arma a distància. Munició limitada  
	- [ ]  Apareixen N enemics en un espai de temps finit.  
	- [ ]  Al morir tots els enemics esperats: joc terminat, mostrem punts
